<?php

return [

    'full_name'                   => 'Nama Lengkap',
    'email'                       => 'Email',
    'password'                    => 'Kata Sandi',
    'retype_password'             => 'Ulangi Kata Sandi',
    'remember_me'                 => 'Ingat Saya',
    'register'                    => 'Register',
    'register_a_new_membership'   => 'Buat akun baru',
    'i_forgot_my_password'        => 'Lupa kata sandi',
    'i_already_have_a_membership' => 'Saya sudah punya akun',
    'sign_in'                     => 'Login',
    'log_out'                     => 'Logout',
    'toggle_navigation'           => 'Toggle navigation',
    'login_message'               => 'Masuk ke sistem',
    'register_message'            => 'Register a new membership',
    'password_reset_message'      => 'Reset Kata Sandi',
    'reset_password'              => 'Reset Password',
    'send_password_reset_link'    => 'Kirim Link Reset Kata Sandi',
    'verify_message'              => 'Akun anda perlu di verifikasi',
    'verify_email_sent'           => 'A fresh verification link has been sent to your email address.',
    'verify_check_your_email'     => 'Before proceeding, please check your email for a verification link.',
    'verify_if_not_recieved'      => 'If you did not receive the email',
    'verify_request_another'      => 'click here to request another',
    'confirm_password_message'    => 'Harap isi konfirmasi kata sandi untuk melanjutkan',
];
