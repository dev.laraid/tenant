<?php

return [

    'main_navigation'               => 'MENU UTAMA',
    'blog'                          => 'Blog',
    'pages'                         => 'Pages',
    'account_settings'              => 'KELOLA AKUN',
    'profile'                       => 'Profile',
    'change_password'               => 'Change Password',
    'multilevel'                    => 'Multi Level',
    'level_one'                     => 'Level 1',
    'level_two'                     => 'Level 2',
    'level_three'                   => 'Level 3',
    'labels'                        => 'LABELS',
    'important'                     => 'Important',
    'warning'                       => 'Warning',
    'information'                   => 'Information',
];
