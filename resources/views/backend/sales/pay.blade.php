@extends('adminlte::page')

@section('title', config('app.name') . ' -')
@section('title_postfix', 'Pengguna')

@section('content_header')
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Pembayaran</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('sale.index') }}">Transaksi</a></li>
        <li class="breadcrumb-item active">Pembayaran</li>
      </ol>
    </div>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card card-success">
        <div class="card-header">
          Form Pembayaran
        </div>
        <div class="card-body">
          <form id="form-pay" action="{{ route('sale.pay.store', $sale->id) }}" method="POST">
            @csrf
            <div class="form-group">
              <label for="name" class="d-block">Total Order</label>
              <input type="text" class="form-control" value="Rp {{ rupiah($sale->price_total) }}" disabled>
            </div>
            <div class="form-group {{ $errors->has('paid') ? ' has-error' : '' }}">
              <label for="paid" class="d-block">Dibayar</label>
              <input type="number" id="paid" class="form-control @if ($errors->has('paid')) is-invalid @endif" name="paid" value="{{ old('paid') }}" onkeyup="calculate()">
              @if ($errors->has('paid'))
                <div class="invalid-feedback">
                  <strong>{{ $errors->first('paid') }}</strong>
                </div>
              @endif
            </div>
            <div class="form-group {{ $errors->has('cashback') ? ' has-error' : '' }}">
              <label for="cashback" class="d-block">Kembalian</label>
              <input type="text" id="cashback" class="form-control @if ($errors->has('cashback')) is-invalid @endif" value="{{ old('cashback') }}" readonly>
              <input type="hidden" id="cashback_real" name="cashback">
              @if ($errors->has('cashback'))
                <div class="invalid-feedback">
                  <strong>{{ $errors->first('cashback') }}</strong>
                </div>
              @endif
            </div>
            <div class="form-group {{ $errors->has('note') ? ' has-error' : '' }}">
              <label for="note" class="d-block">Dibayar Terbilang</label>
              <div class="display-4 terbilang">-</div>
            </div>
            <button type="button" onclick="pay()" class="btn btn-success"><i class="fa fa-save"></i> Simpan Pembayaran</button>
          </form>
        </div>
      </div>
    </div>

    <div class="col-md-3">
      <div class="card">
        <div class="card-header">
          Informasi Order
        </div>
        <div class="card-body">
          <form action="" method="POST">
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
              <label for="name" class="d-block">Nama</label>
              <input type="text" id="customer_name" class="form-control @if ($errors->has('name')) is-invalid @endif" value="{{ $sale->customer_name }}" disabled>
              @if ($errors->has('name'))
                <div class="invalid-feedback">
                  <strong>{{ $errors->first('name') }}</strong>
                </div>
              @endif
            </div>
            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
              <label for="phone" class="d-block">Nomor Whatsapp</label>
              <input type="number" id="customer_phone" class="form-control @if ($errors->has('phone')) is-invalid @endif" value="{{ $sale->customer_phone }}" disabled>
              @if ($errors->has('phone'))
                <div class="invalid-feedback">
                  <strong>{{ $errors->first('phone') }}</strong>
                </div>
              @endif
            </div>
            <div class="form-group {{ $errors->has('note') ? ' has-error' : '' }}">
              <label for="note" class="d-block">Catatan</label>
              <textarea id="customer_note" class="form-control @if ($errors->has('note')) is-invalid @endif" rows="2" disabled>{{ $sale->customer_note }}</textarea>
              @if ($errors->has('note'))
                <div class="invalid-feedback">
                  <strong>{{ $errors->first('note') }}</strong>
                </div>
              @endif
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="col-md-9">
      <div class="card">
        <div class="card-header">
          Beli dari : &nbsp;<strong>{{ $sale->tenant->name }}</strong>
        </div>
        <div class="card-body p-0">
          <div class="table-responsive">
            <table class="table table-hover shopping-cart-wrap">
              <thead class="text-muted">
                <tr>
                  <th scope="col">Nama Item</th>
                  <th scope="col" width="150">Jumlah</th>
                  <th scope="col" width="180" class="text-right">Total Harga</th>
                </tr>
              </thead>
              <tbody>
                @php $subtotal = 0; @endphp
                @foreach ($sale->details as $item)
                  @php
                    $price_total = ((int) $item->price) * $item->qty;
                    $subtotal += $price_total;
                  @endphp
                  <tr>
                    <td>
                      <figure class="media mt-2">
                        <div class="img-wrap"><img src="{{ asset('uploads/images/products/'.$item->food->image) }}" class="center-cropped img-rounded"></div>
                        <figcaption class="media-body">
                          <h6 class="title text-truncate">{{ $item->food->name }}</h6>
                          <dl class="param param-inline small text-danger">
                            <dt>Harga : </dt>
                            <dd>Rp {{ rupiah((int) $item->price) }}</dd>
                          </dl>
                        </figcaption>
                      </figure> 
                    </td>
                    <td class="align-middle"> 
                      <span class="text-success text-bold">{{ $item->qty }}</span>
                    </td>
                    <td class="align-middle text-right"> 
                      <div class="price-wrap"> 
                        <var class="price">Rp {{ rupiah($price_total) }}</var> 
                      </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
              <tfoot>
                @if ($sale->details)
                  <tr>
                    <th class="text-right" colspan="2">Total</th>
                    <td class="text-right" colspan="2">
                      <div class="price-wrap"> 
                        <var class="price mr-3">Rp {{ rupiah($subtotal) }}</var> 
                      </div>
                    </td>
                  </tr>
                @endif
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('css')
    <style>
      .center-cropped {
        background-position: center center;
        background-repeat: no-repeat;
        overflow: hidden;
        height: 50px;
        width: 65px;
      }

      .param {
          margin-bottom: 7px;
          line-height: 1.4;
      }
      .param-inline dt {
          display: inline-block;
      }
      .param dt {
          margin: 0;
          margin-right: 7px;
          font-weight: 600;
      }
      .param-inline dd {
          vertical-align: baseline;
          display: inline-block;
      }

      .param dd {
          margin: 0;
          vertical-align: baseline;
      } 

      .shopping-cart-wrap .price {
          color: #007bff;
          font-size: 15px;
          font-weight: bold;
          margin-right: 5px;
          display: block;
      }
      var {
          font-style: normal;
      }

      .media img {
          margin-right: 1rem;
      }
      .img-sm {
          width: 90px;
          max-height: 75px;
          object-fit: cover;
      }
    </style>
@endsection

@section('js')
  <script src="{{ asset('assets/js/terbilang.js') }}"></script>
  <script>
    $(document).ready(function () {
      bsCustomFileInput.init()
      $('.select2').select2();
      $('#paid').focus();
    })

    function pay() {
      let bill = {{ $sale->price_total }};
      let val = $('#paid').val();

      if (val < bill) {
        swal(
          'Gagal!',
          'Jumlah pembayaran masih kurang!',
          'error'
        );
        $('#paid').focus();
        return;
      }

      $('#form-pay').submit();
    }

    function numberFormat(x) {
      return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
    }

    function calculate() {
      let val = $('#paid').val();
      let cb = 0;
      let bill = {{ $sale->price_total }};
      let result = terbilang(val);
      $('.terbilang').html(result);
      cb = val - bill;
      if (cb > 0) {
        $('#cashback').val('Rp ' + numberFormat(cb));
        $('#cashback_real').val(cb);
      } else {
        $('#cashback_real').val(0);
        $('#cashback').val('-');
      }
    }

  </script>
@endsection