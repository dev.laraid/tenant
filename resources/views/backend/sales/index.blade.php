@extends('adminlte::page')

@section('title', config('app.name') . ' -')
@section('title_postfix', 'Transaksi')

@section('content_header')
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Transaksi</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item active">Transaksi</li>
      </ol>
    </div>
  </div>

  <div class="d-sm-flex align-items-center justify-content-start">
    {{-- <a class="btn btn-sm btn-secondary mr-auto" href="{{ route('user.create') }}"><i class="fa fa-plus"></i> Buat Akun</a>  --}}
    <div class="form-inline ml-auto">
      <label>Filter Status</label>
      <select name="status" class="form-control-sm ml-2">
        <option value="all">Semua</option>
        <option value="100">Menunggu Dibayar</option>
        <option value="200">Dibayar</option>
        <option value="10">Dibatalkan</option>
      </select>
      <button class="btn btn-sm btn-secondary ml-2" id="btn-filter"><i class="fas fa-filter"></i></button>
    </div>
  </div>
@endsection

@section('content')
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">List Transaksi</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
          <i class="fas fa-times"></i></button>
      </div>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered datatable">
          <thead>                                 
            <tr>
              <th>#</th>
              <th>Invoice</th>
              <th>Tenant</th>
              <th>Customer</th>
              <th>Total Bayar</th>
              <th>Status</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('js')
  <script>
      $(document).ready(function() {
        $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            language: {
              url: '{{ asset('vendor/datatables/lang/Indonesian.json') }}'
            },
            ajax: {
              url: '{{ route('sale.index') }}',
              data: function (d) {
                d.status = $('select[name=status]').val()
              }
            },
            columns: [
              {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
              {data: 'invoice', name: 'invoice'},
              {data: 'tenant.name', name: 'tenant.name'},
              {data: 'customer_name', name: 'customer_name', defaultContent : '-'},
              {data: null, name: 'price_total', render: function ( data, type, row ) {
                return 'Rp ' + numberFormat(data.price_total);
              }},
              {data: null, name: 'status', render: function ( data, type, row ) {
                var badge = 'badge-warning';
                if (data.status == 200){
                  badge = 'badge-success';  
                } else if (data.status == 300){
                  badge = 'badge-primary';  
                } else if (data.status == 10){
                  badge = 'badge-danger';  
                } 
                return '<span class="badge '+badge+'">'+data.display_status+'</span>';
              }},
              {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        $('#btn-filter').click(function(){
          $('.datatable').DataTable().draw(true);
        });

        $(document).on('click','.js-submit-confirm', function(e){
            e.preventDefault();
            swal({
              title: 'Apakah anda yakin ingin menghapus?',
              text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
              icon: 'warning',
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                $(this).closest('form').submit();
              } 
            });
        });
    });

    function numberFormat(x) {
      return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
    }
  </script>
  @include('partials._toast')
@endsection