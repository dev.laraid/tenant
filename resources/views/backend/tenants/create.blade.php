@extends('adminlte::page')

@section('title', config('app.name') . ' -')
@section('title_postfix', 'Pengguna')

@section('content_header')
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Tenant</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('tenant.index') }}">Tenant</a></li>
        <li class="breadcrumb-item active">Tambah Tenant</li>
      </ol>
    </div>
  </div>
@endsection

@section('content')
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Tambah Tenant</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
          <i class="fas fa-times"></i></button>
      </div>
    </div>
    <div class="card-body">
      <form method="POST" action="{{ route('tenant.store') }}" enctype="multipart/form-data">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold">Informasi Tenant</h6>
              </div>
              <div class="card-body">
                {{ csrf_field() }}

                <div class="form-group {{ $errors->has('user_id') ? ' has-error' : '' }}">
                  <label for="user_id">Pemilik</label>
                  <select name="user_id" class="form-control select2 @if ($errors->has('user_id')) is-invalid @endif" data-placeholder="Pilih Pemilik">
                    <option></option>
                    @foreach ($user as $item)
                      <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('user_id'))
                    <div class="invalid-feedback">
                      {{ $errors->first('user_id') }}
                    </div>
                  @endif
                </div>

                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="name">Nama Tenant</label>
                  <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" value="{{ old('name') }}" placeholder="Nama Tenant">
                  @if ($errors->has('name'))
                    <div class="invalid-feedback">
                      {{ $errors->first('name') }}
                    </div>
                  @endif
                </div>

                <div class="row">
                  <div class="form-group col-12{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description">Deskripsi</label>
                    <textarea id="description" class="form-control @if ($errors->has('description')) is-invalid @endif" name="description" rows="3" placeholder="Deskripsi">{{ old('description') }}</textarea>
                    @if ($errors->has('description'))
                      <div class="invalid-feedback">
                        {{ $errors->first('description') }}
                      </div>
                    @endif
                  </div>
                </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-secondary btn-block" tabindex="4">
                    Simpan
                  </button>
                </div>
                
              </div>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="card">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold">Foto Tenant</h6>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <div class="text-center">
                    <img src="{{ asset('assets/img/placeholder-image.png') }}" class="rounded-circle" id="logo-prev" width="168" height="168" alt="tenant">
                  </div>
                </div>
                <div class="form-group custom-file mb-3">
                  <input id="logo" type="file" class="custom-file-input {{ $errors->has('image') ? ' has-error' : '' }}" name="image">
                  <label class="custom-file-label" for="customFile">Pilih Gambar</label>
                </div>
                @if ($errors->has('image'))
                  <div class="invalid-feedback">
                    <strong>{{ $errors->first('image') }}</strong>
                  </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection

@section('js')
  <script>
    $(document).ready(function () {
      bsCustomFileInput.init()
      $('.select2').select2();
    })

    function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#logo-prev').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }

    $("#logo").change(function() {
      readURL(this);
    });
  </script>
@endsection