@extends('adminlte::page')

@section('title', config('app.name') . ' -')
@section('title_postfix', 'Tenant')

@section('content_header')
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Tenant</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('tenant.index') }}">Tenant</a></li>
        <li class="breadcrumb-item active">Lihat Tenant</li>
      </ol>
    </div>
  </div>
@endsection

@section('content')
  <div class="card-body">
    <div class="row">
      <div class="col-lg-6">
        <div class="card">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">Detail Tenant</h6>
          </div>
          <div class="card-body">
            <div>
              Nama<br>
              <strong>{{ $tenant->name }}</strong>
            </div>
            <hr>
            <div>
              Deskripsi <br>
              <strong>{{ $tenant->description }}</strong>
            </div>
            <hr>
            <div>
              Status Verifikasi <br>
              <div class="row">
                <div class="col-md-6">
                  @if ($tenant->is_verification == 1)
                    <span class="badge badge-success">Terverifikasi</span>
                  @else
                    <span class="badge badge-warning">Belum diverifikasi</span>
                  @endif
                </div>
                <div class="col-md-6 text-right">
                  @if ($tenant->is_verification == 0)  
                    <button class="btn btn-success btn-sm" id="btn-verification"><i class="fa fa-check"></i> Klik untuk verifikasi</button>
                  @endif
                </div>
              </div>
            </div>
            <hr>
            <div>
              Saldo Tersimpan <br>
              <strong>Rp {{ rupiah($tenant->balances) }}</strong>
            </div>
            <hr>
            <div>
              Logo <br><br>
              @if ($tenant->image == null)
                <img src="{{ asset('assets/img/placeholder-image.png') }}" class="rounded" width="168" alt="image-logo">  
              @else
                <img alt="image" src="{{ asset('uploads/images/tenant/'.$tenant->image) }}" class="rounded" width="168" alt="image-logo">
              @endif
            </div>

          </div>
        </div>
      </div>

      <div class="col-lg-6">
        <div class="card">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">Informasi Pemilik</h6>
          </div>
          <div class="card-body">
            <div>
              Nama<br>
              <strong>{{ $tenant->user->name }}</strong>
            </div>
            <hr>
            <div>
              Email <br>
              <strong>{{ $tenant->user->email }}</strong>
            </div>
            <hr>
            <div>
              Nomor HP <br>
              <strong>{{ $tenant->user->phone }}</strong>
            </div>
            <hr>
            <div>
              Foto Pemilik <br><br>
              @if (is_null($tenant->user->avatar))
                <img src="{{ asset('assets/img/avatar.jpg') }}" class="rounded-circle" id="avatar-prev" width="168" height="168" alt="avatar">
              @else
                <img alt="image" src="{{asset('uploads/images/avatars/'.$tenant->user->avatar)}}" class="rounded-circle" id="avatar-prev" width="168" height="168" alt="avatar">
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <form action="{{ route('tenant.verification') }}" method="post" id="form-verif">
    @csrf
    <input type="hidden" name="tokenizer" value="{{ $id_crypt }}">
  </form>
@endsection

@section('js')
  <script>
   $('#btn-verification').on('click', function() {
    swal({
      title: 'Apakah anda yakin?',
      text: 'Akan melakukan verifikasi untuk tenant ini?',
      icon: 'info',
      buttons: true,
    })
    .then((confirm) => {
      if (confirm) {
        $('#form-verif').submit();
      } 
    });
  });
  </script>
  @include('partials._toast')
@endsection