@extends('adminlte::page')

@section('title', config('app.name') . ' -')
@section('title_postfix', 'Tenant')

@section('content_header')
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Tenant</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item active">Tenant</li>
      </ol>
    </div>
  </div>

  <div class="d-sm-flex align-items-center justify-content-start">
    <a class="btn btn-sm btn-secondary mr-auto" href="{{ route('tenant.create') }}"><i class="fa fa-plus"></i> Daftarkan Tenant</a> 
    <div class="form-inline ml-auto">
      <label>Filter Status Verifikasi</label>
      <select name="is_verification" class="form-control-sm ml-2">
        <option value="all">Semua</option>
        <option value="1">Terverifikasi</option>
        <option value="0">Belum Terverifikasi</option>
      </select>
      <button class="btn btn-sm btn-secondary ml-2" id="btn-filter"><i class="fas fa-filter"></i></button>
    </div>
  </div>
@endsection

@section('content')
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">List Tenant</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
          <i class="fas fa-times"></i></button>
      </div>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered datatable">
          <thead>                                 
            <tr>
              <th>#</th>
              <th>Nama</th>
              <th>Pemilik</th>
              <th>Deskripsi</th>
              <th>Verifikasi</th>
              <th>Status</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('js')
  <script>
      $(document).ready(function() {
        $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: false,
            language: {
                url: '{{ asset('vendor/datatables/lang/Indonesian.json') }}'
            },
            ajax: {
              url: '{{ route('tenant.index') }}',
              data: function (d) {
                d.is_verification = $('select[name=is_verification]').val()
              }
            },
            columns: [
              {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
              {data: 'name', name: 'name'},
              {data: 'user.name', name: 'user.name'},
              {data: 'description', name: 'description', defaultContent : '-'},
              {data: null, name: 'is_verification', render: function ( data, type, row ) {
                var badge = 'badge-warning';
                if (data.is_verification == 1){
                  badge = 'badge-success';  
                  return '<span class="badge '+badge+'">Terverifikasi</span>';
                } else {
                  return '<span class="badge '+badge+'">Belum Terverifikasi</span>';
                }
              }},
              {data: null, name: 'status', render: function ( data, type, row ) {
                var badge = 'badge-danger';
                if (data.status == 100){
                  badge = 'badge-success';  
                  return '<span class="badge '+badge+'">'+data.display_status+'</span>';
                } else {
                  return '<span class="badge '+badge+'">'+data.display_status+'</span>';
                }
              }},
              {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        $('#btn-filter').click(function(){
          $('.datatable').DataTable().draw(true);
        });

        $(document).on('click','.js-submit-confirm', function(e){
            e.preventDefault();
            swal({
              title: 'Apakah anda yakin ingin menghapus?',
              text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
              icon: 'warning',
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                $(this).closest('form').submit();
              } 
            });
        });
    });

    const capitalize = (s) => {
      if (typeof s !== 'string') return ''
      return s.charAt(0).toUpperCase() + s.slice(1)
    }
  </script>
  @include('partials._toast')
@endsection