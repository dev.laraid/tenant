@extends('adminlte::page')

@section('title', config('app.name') . ' -')
@section('title_postfix', 'Pengguna')

@section('content_header')
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Tenant</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('tenant.index') }}">Tenant</a></li>
        <li class="breadcrumb-item active">Ubah Tenant</li>
      </ol>
    </div>
  </div>
@endsection

@section('content')
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Ubah Tenant</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
          <i class="fas fa-times"></i></button>
      </div>
    </div>
    <div class="card-body">
      <form method="POST" action="{{ route('tenant.update', $tenant->id) }}" enctype="multipart/form-data">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold">Informasi Tenant</h6>
              </div>
              <div class="card-body">
                {{ csrf_field() }}
                @method('PUT')

                <div class="form-group {{ $errors->has('user_id') ? ' has-error' : '' }}">
                  <label for="user_id">Pemilik</label>
                  <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" value="{{ $tenant->user->name }}" placeholder="Nama Pemilik" readonly>
                  @if ($errors->has('user_id'))
                    <div class="invalid-feedback">
                      {{ $errors->first('user_id') }}
                    </div>
                  @endif
                </div>

                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                  <label for="name">Nama Tenant</label>
                  <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" value="{{ $tenant->name }}" placeholder="Nama Tenant">
                  @if ($errors->has('name'))
                    <div class="invalid-feedback">
                      {{ $errors->first('name') }}
                    </div>
                  @endif
                </div>

                <div class="row">
                  <div class="form-group col-12{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description">Deskripsi</label>
                    <textarea id="description" class="form-control @if ($errors->has('description')) is-invalid @endif" name="description" rows="3" placeholder="Deskripsi">{{ $tenant->description }}</textarea>
                    @if ($errors->has('description'))
                      <div class="invalid-feedback">
                        {{ $errors->first('description') }}
                      </div>
                    @endif
                  </div>
                </div>

                <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                  <label for="status">Status Tenant</label>
                  <select name="status" class="form-control select2 @if ($errors->has('status')) is-invalid @endif">
                    <option value="100" {{ $tenant->status == '100' ? 'selected' : '' }}>Aktif</option>
                    <option value="10" {{ $tenant->status == '10' ? 'selected' : '' }}>Tidak Aktif</option>
                  </select>
                  @if ($errors->has('status'))
                    <div class="invalid-feedback">
                      {{ $errors->first('status') }}
                    </div>
                  @endif
                </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-secondary btn-block" tabindex="4">
                    Simpan
                  </button>
                </div>
                
              </div>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="card">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold">Foto Tenant</h6>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <div class="text-center">
                    @if ($tenant->image == null)
                      <img src="{{ asset('assets/img/placeholder-image.png') }}" class="rounded" width="168" alt="image-logo">  
                    @else
                      <img alt="image" src="{{ asset('uploads/images/tenant/'.$tenant->image) }}" class="rounded" width="168" alt="image-logo">
                    @endif
                  </div>
                </div>
                <div class="form-group custom-file mb-3">
                  <input id="logo" type="file" class="custom-file-input {{ $errors->has('image') ? ' has-error' : '' }}" name="image">
                  <label class="custom-file-label" for="customFile">Pilih Gambar</label>
                </div>
                @if ($errors->has('image'))
                  <div class="invalid-feedback">
                    <strong>{{ $errors->first('image') }}</strong>
                  </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection

@section('js')
  <script>
    $(document).ready(function () {
      bsCustomFileInput.init()
      $('.select2').select2();
    })

    function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#logo-prev').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }

    $("#logo").change(function() {
      readURL(this);
    });
  </script>
@endsection