
@extends('layouts.main')

@section('title', config('app.name'))

@section('content_header')
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1 class="text-white">Keranjang Order</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Beranda</a></li>
        <li class="breadcrumb-item text-white active">Keranjang</li>
      </ol>
    </div>
  </div>
@endsection

@section('content')
  <div class="row">
    @if (count($carts) > 0)    
      <div class="col-md-3">
        <div class="card">
          <div class="card-header">
            Informasi Order
          </div>
          <div class="card-body">
            <form action="" method="POST">
              <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="d-block">Nama</label>
                <input type="text" id="customer_name" class="form-control @if ($errors->has('name')) is-invalid @endif" value="{{ old('name') }}" onchange="setOrderInfo()">
                @if ($errors->has('name'))
                  <div class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                <label for="phone" class="d-block">Nomor Whatsapp</label>
                <input type="number" id="customer_phone" class="form-control @if ($errors->has('phone')) is-invalid @endif" value="{{ old('phone') }}" onchange="setOrderInfo()">
                @if ($errors->has('phone'))
                  <div class="invalid-feedback">
                    <strong>{{ $errors->first('phone') }}</strong>
                  </div>
                @endif
              </div>
              {{-- <div class="form-group {{ $errors->has('note') ? ' has-error' : '' }}">
                <label for="note" class="d-block">Catatan</label>
                <textarea id="customer_note" class="form-control @if ($errors->has('note')) is-invalid @endif" rows="2" onchange="setOrderInfo()">{{ old('note') }}</textarea>
                @if ($errors->has('note'))
                  <div class="invalid-feedback">
                    <strong>{{ $errors->first('note') }}</strong>
                  </div>
                @endif
              </div> --}}
            </form>
            <div class="alert alert-secondary">
              <h5><i class="icon fas fa-info"></i> Info</h5>
              Harap masukan nomor whatsapp yang benar, order akan diinformasikan melalui whatsapp :)
            </div>
          </div>
        </div>
      </div>
    @endif

    <div class="col-md-9">
      @forelse ($carts as $key => $cart)
        <div class="card">
          <div class="card-header">
            Beli dari : &nbsp;<strong>{{ $cart['tenant_name'] }}</strong>
          </div>
          <div class="card-body p-0">
            <div class="table-responsive">
              <table class="table table-hover shopping-cart-wrap">
                <thead class="text-muted">
                  <tr>
                    <th scope="col">Nama Item</th>
                    <th scope="col" width="150">Jumlah</th>
                    <th scope="col" width="180">Total Harga</th>
                    <th scope="col" width="180">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @php $subtotal = 0; @endphp
                  @foreach ($cart['data'] as $item)
                    @php
                      $price_total = ((int) $item['price']) * $item['quantity'];
                      $subtotal += $price_total;
                    @endphp
                    <tr>
                      <td>
                        <figure class="media mt-2">
                          <div class="img-wrap"><img src="{{ $item['image_url'] }}" class="center-cropped img-rounded"></div>
                          <figcaption class="media-body">
                            <h6 class="title text-truncate">{{ $item['name'] }}</h6>
                            <dl class="param param-inline small text-danger">
                              <dt>Harga : </dt>
                              <dd>Rp {{ rupiah((int) $item['price']) }}</dd>
                            </dl>
                          </figcaption>
                        </figure> 
                      </td>
                      <td class="align-middle"> 
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <button class="btn btn-dark btn-sm" id="minus-btn" onclick="decQty({{ $item['id'] }})"><i class="fa fa-minus"></i></button>
                          </div>
                          <input type="number" id="qty_input_{{ $item['id'] }}" class="form-control form-control-sm text-center h5 qty_input" value="{{ $item['quantity'] }}" min="1">
                          <div class="input-group-prepend">
                            <button class="btn btn-dark btn-sm" id="plus-btn" onclick="incQty({{ $item['id'] }})"><i class="fa fa-plus"></i></button>
                          </div>
                        </div>
                      </td>
                      <td class="align-middle"> 
                        <div class="price-wrap"> 
                          <var class="price">Rp {{ rupiah($price_total) }}</var> 
                        </div>
                      </td>
                      <td class="align-middle"> 
                        <a href="javascript:void(0)" onclick="updateItem({{ $key }}, {{ $item['id'] }})" class="btn btn-sm btn-outline-warning">Update</a>
                        <a href="javascript:void(0)" onclick="removeItem({{ $key }}, {{ $item['id'] }})" class="btn btn-sm btn-outline-danger">Hapus</a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  @if ($cart)
                    <tr>
                      <th class="text-right" colspan="3">Total</th>
                      <td class="text-right" colspan="2">
                        <div class="price-wrap"> 
                          <var class="price">Rp {{ rupiah($subtotal) }}</var> 
                        </div>
                      </td>
                    </tr>
                  @endif
                </tfoot>
              </table>
            </div>
          </div>
          @if ($cart)
            <div class="card-footer text-right">
              <form id="frm-checkout" action="{{ route('checkout.store', $key) }}" method="post">
                @csrf
                <input type="hidden" id="c_name" name="name" value="">
                <input type="hidden" id="c_phone" name="phone" value="">
                {{-- <input type="hidden" id="c_note" name="note" value="aaa"> --}}

                <button type="submit" class="btn btn-primary"><i class="fa fa-chevron-right"></i> Order Sekarang</button>
              </form>
            </div>
          @endif
        </div>
      @empty
        <div class="text-center">
          Tidak ada item dikeranjang :(
        </div>
      @endforelse
    </div>
  </div>

  <form id="frm-remove" action="{{ route('cart.remove') }}" method="post">
    @csrf
    <input type="hidden" id="tenant_id" name="tenant_id">
    <input type="hidden" id="food_id" name="food_id">
  </form>

  <form id="frm-update" action="{{ route('cart.update') }}" method="post">
    @csrf
    <input type="hidden" id="tent_id" name="tenant_id">
    <input type="hidden" id="fd_id" name="food_id">
    <input type="hidden" id="quantity" name="quantity">
  </form>
  
@endsection

@section('css')
    <style>
      .center-cropped {
        background-position: center center;
        background-repeat: no-repeat;
        overflow: hidden;
        height: 50px;
        width: 65px;
      }

      .param {
          margin-bottom: 7px;
          line-height: 1.4;
      }
      .param-inline dt {
          display: inline-block;
      }
      .param dt {
          margin: 0;
          margin-right: 7px;
          font-weight: 600;
      }
      .param-inline dd {
          vertical-align: baseline;
          display: inline-block;
      }

      .param dd {
          margin: 0;
          vertical-align: baseline;
      } 

      .shopping-cart-wrap .price {
          color: #007bff;
          font-size: 15px;
          font-weight: bold;
          margin-right: 5px;
          display: block;
      }
      var {
          font-style: normal;
      }

      .media img {
          margin-right: 1rem;
      }
      .img-sm {
          width: 90px;
          max-height: 75px;
          object-fit: cover;
      }
    </style>
@endsection

@section('js')
    <script> 
      $(document).ready(function(){
        $('.qty_input').prop('disabled', true);
        $('.select2').select2();
        getOrderInfo();
      });

      var incQty = function(id) {
        let $input = $('#qty_input_'+id);
        let qty = parseInt($input.val()) + 1 
        if (qty > 30) {
          swal(
              'Maaf!',
              'Maximum jumlah item adalah 30 pcs',
              'error'
          );
          return;
        }
        $input.val(qty);
      }

      var decQty = function(id) {
        let $input = $('#qty_input_'+id);
        $input.val(parseInt($input.val()) - 1 );
        if ($input.val() == 0) {
          $input.val(1);
        }
      }

      function removeItem(t_id, f_id) {
        $('#tenant_id').val(t_id);
        $('#food_id').val(f_id);

        $('#frm-remove').submit();
      }

      function updateItem(t_id, f_id) {
        $('#tent_id').val(t_id);
        $('#fd_id').val(f_id);
        $('#quantity').val($('#qty_input_'+f_id).val());

        $('#frm-update').submit();
      }

      function setOrderInfo() {
        var name = $('#customer_name').val();
        var phone = $('#customer_phone').val();
        //var note = $('#customer_note').val();
        var obj = {
          name: name,
          phone: phone,
          //note: note,
        };

        localStorage.setItem("orderInfoT3n4nt", JSON.stringify(obj));
        getOrderInfo();
      }

      function getOrderInfo() {
        var obj = JSON.parse(localStorage.getItem("orderInfoT3n4nt"));
        if (obj != null) {
          var name = obj.name;
          var phone = obj.phone;
          //var note = obj.note;

          $('#customer_name').val(name);
          $('#customer_phone').val(phone);
          //$('#customer_note').val(note);
          $('#c_name').val(name);
          $('#c_phone').val(phone);
          $('#c_note').val(note);
        }
        //console.log(obj);
      }

      @if(Session::has('swal_notification.message'))
        var type = "{{ Session::get('swal_notification.level', 'info') }}";
        switch(type){
          case 'success':
            swal(
              'Sukses!',
              '{{ Session::get('swal_notification.message') }}',
              'success'
            );
            break;

          case 'error':
            swal(
              'Gagal!',
              '{{ Session::get('swal_notification.message') }}',
              'error'
            );
            break;

          default:
            break;
        }
      @endif

    </script>
@endsection