@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Dashboard</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">Dashboard</li>
      </ol>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-2">
      <!-- small box -->
      <div class="small-box bg-info">
        <div class="inner">
          <h3>{{ $data['product'] }}</h3>

          <p>Produk</p>
        </div>
        <div class="icon">
          <i class="fas fa-fw fa-hamburger"></i>
        </div>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-2">
      <!-- small box -->
      <div class="small-box bg-success">
        <div class="inner">
          <h3>{{ $data['order'] }}</h3>

          <p>Order</p>
        </div>
        <div class="icon">
          <i class="fas fa-fw fa-receipt"></i>
        </div>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-4">
      <!-- small box -->
      <div class="small-box bg-secondary">
        <div class="inner">
          <h3>Rp {{ $data['income_today'] }}</h3>

          <p>Pendapatan Hari Ini</p>
        </div>
        <div class="icon">
          <i class="fas fa-fw fa-download"></i>
        </div>
      </div>
    </div>
    <!-- ./col -->
    <!-- ./col -->
    <div class="col-lg-4">
      <!-- small box -->
      <div class="small-box bg-primary">
        <div class="inner">
          <h3>Rp {{ $data['income_all'] }}</h3>

          <p>Pendapatan Keseluruhan</p>
        </div>
        <div class="icon">
          <i class="fas fa-fw fa-download"></i>
        </div>
      </div>
    </div>
    <!-- ./col -->
  </div>
@endsection

@section('content')
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Selamat Datang - {{ Auth::user()->tenant->name }}</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
          <i class="fas fa-times"></i></button>
      </div>
    </div>
    <div class="card-body">
      Anda login sebagai <strong>{{ Auth::user()->name }}</strong>
    </div>
  </div>
@endsection

@section('css')
    {{-- <link rel="stylesheet" href="/css/admin_custom.css"> --}}
@endsection

@section('js')
    <script> console.log('Hi!'); </script>
@endsection