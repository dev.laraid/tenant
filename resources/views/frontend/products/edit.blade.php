@extends('adminlte::page')

@section('title', config('app.name') . ' -')
@section('title_postfix', 'Produk')

@section('content_header')
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Produk</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{ route('product.index') }}">Produk</a></li>
        <li class="breadcrumb-item active">Edit Produk</li>
      </ol>
    </div>
  </div>
@endsection

@section('content')
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Edit Produk</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
          <i class="fas fa-times"></i></button>
      </div>
    </div>
    <div class="card-body">
      <form method="POST" action="{{ route('product.update', $product->id) }}" enctype="multipart/form-data">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold">Informasi Produk</h6>
              </div>
              <div class="card-body">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="row">
                  <div class="form-group col-6 {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">Nama</label>
                    <input id="name" type="text" class="form-control @if ($errors->has('name')) is-invalid @endif" name="name" tabindex="1" value="{{ $product->name }}">
                    @if ($errors->has('name'))
                      <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                      </div>
                    @endif
                  </div>
                  
                  <div class="form-group col-6 {{ $errors->has('type') ? ' has-error' : '' }}">
                    <label for="type">Jenis</label>
                    <select name="type" class="form-control select2">
                      <option value="makanan" {{ $product->type == 'makanan' ? 'selected' : '' }}>Makanan</option>
                      <option value="minuman" {{ $product->type == 'minuman' ? 'selected' : '' }}>Minuman</option>
                      <option value="ice" {{ $product->type == 'ice' ? 'selected' : '' }}>Ice Cream</option>
                    </select>
                    @if ($errors->has('type'))
                      <div class="invalid-feedback">
                        {{ $errors->first('type') }}
                      </div>
                    @endif
                  </div>
                </div>

                <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                  <label for="description">Deskripsi</label>
                  <input id="description" type="text" class="form-control @if ($errors->has('description')) is-invalid @endif" name="description" tabindex="1" value="{{ $product->description }}">
                  @if ($errors->has('description'))
                    <div class="invalid-feedback">
                      {{ $errors->first('description') }}
                    </div>
                  @endif
                </div>

                <div class="form-group {{ $errors->has('price') ? ' has-error' : '' }}">
                  <label for="price">Harga</label>
                  <input id="price" type="number" class="form-control @if ($errors->has('price')) is-invalid @endif" name="price" tabindex="1" value="{{ $product->price }}">
                  @if ($errors->has('price'))
                    <div class="invalid-feedback">
                      {{ $errors->first('price') }}
                    </div>
                  @endif
                </div>

                <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                  <label for="status">Status</label>
                  <select name="status" class="form-control select2">
                    <option value="100" {{ $product->status == '100' ? 'selected' : '' }}>Aktif</option>
                    <option value="10" {{ $product->status == '10' ? 'selected' : '' }}>Non Aktif</option>
                  </select>
                  @if ($errors->has('status'))
                    <div class="invalid-feedback">
                      {{ $errors->first('status') }}
                    </div>
                  @endif
                </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-secondary btn-block" tabindex="4">
                    Simpan
                  </button>
                </div>
                
              </div>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="card">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold">Foto Produk</h6>
              </div>
              <div class="card-body">
                <div class="form-group">
                  <div class="text-center">
                    @if (is_null($product->image))
                      <img src="{{ asset('assets/img/placeholder-image.png') }}" class="rounded" id="image-prev" width="200" height="200" alt="image">
                    @else
                      <img src="{{asset('uploads/images/products/'.$product->image)}}" class="rounded" id="image-prev" width="200" height="200" alt="image">
                    @endif
                  </div>
                </div>
                <div class="form-group custom-file mb-3 {{ $errors->has('image') ? ' has-error' : '' }}">
                  <input id="image" type="file" class="custom-file-input {{ $errors->has('image') ? ' has-error' : '' }}" name="image">
                  <label class="custom-file-label" for="customFile">Pilih Gambar</label>
                  @if ($errors->has('image'))
                    <div class="invalid-feedback">
                      <strong>{{ $errors->first('image') }}</strong>
                    </div>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection

@section('js')
  <script>
    $(document).ready(function () {
      bsCustomFileInput.init()
      $('.select2').select2();
    })

    function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#image-prev').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }

    $("#image").change(function() {
      readURL(this);
    });
  </script>
@endsection