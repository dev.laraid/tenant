@extends('adminlte::page')

@section('title', config('app.name') . ' -')
@section('title_postfix', 'Penjualan')

@section('content_header')
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Penjualan</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
        <li class="breadcrumb-item active">Penjualan</li>
      </ol>
    </div>
  </div>

  <div class="row">
    <div class="col-12 col-sm-6 col-md-3">
      <div class="info-box">
        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-clock"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Belum Dibayar</span>
          <span class="info-box-number">{{ $data['pending'] }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-12 col-sm-6 col-md-3">
      <div class="info-box mb-3">
        <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-cog"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Perlu Diproses</span>
          <span class="info-box-number">{{ $data['paid'] }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix hidden-md-up"></div>

    <div class="col-12 col-sm-6 col-md-3">
      <div class="info-box mb-3">
        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-check-circle"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Order Selesai</span>
          <span class="info-box-number">{{ $data['done'] }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-12 col-sm-6 col-md-3">
      <div class="info-box mb-3">
        <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-times-circle"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Order Dibatalkan</span>
          <span class="info-box-number">{{ $data['reject'] }}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
  </div>

  <div class="d-sm-flex align-items-center justify-content-start">
    {{-- <a class="btn btn-sm btn-secondary mr-auto" href="#" onclick="$('#sendNotifModal').modal('show');"><i class="fa fa-plus"></i> Test</a>  --}}
    <div class="form-inline ml-auto">
      <label>Filter Status</label>
      <select name="status" class="form-control-sm ml-2">
        <option value="all">Semua</option>
        <option value="100">Menunggu Dibayar</option>
        <option value="200">Dibayar - Perlu Diproses</option>
        <option value="10">Dibatalkan</option>
      </select>
      <button class="btn btn-sm btn-secondary ml-2" id="btn-filter"><i class="fas fa-filter"></i></button>
    </div>
  </div>
@endsection

@section('content')
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">List Penjualan</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
          <i class="fas fa-minus"></i></button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
          <i class="fas fa-times"></i></button>
      </div>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered datatable">
          <thead>                                 
            <tr>
              <th>#</th>
              <th>Invoice</th>
              <th>Tenant</th>
              <th>Customer</th>
              <th>Total Bayar</th>
              <th>Status</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="modal fade" tabindex="-1" id="sendNotifModal" role="dialog" aria-labelledby="sendNotifModal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Kirim notifikasi ke pelanggan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="frm-notif" action="https://api.whatsapp.com/send" target="_blank method="GET">
          <div class="modal-body">
            <div class="form-group">
              <label for="text">Pesan notifikasi</label>
              <textarea id="text" class="form-control" name="text" rows="5" placeholder="Isi Pesan">{{ Session::get('order_notification.message') }}</textarea>
            </div>
          </div>
          <input type="hidden" name="phone" id="phone-number" value="{{ Session::get('order_notification.phone') }}">
        </form>
        <div class="modal-footer">
          <a href="javascript:void(0)" type="button" onclick="sendNotif()" id="btn-a2c" class="btn btn-success"><i class="fa fa-paper-plane"></i> Kirim pesan</a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js')
  <script>
    $(document).ready(function() {
      $('.datatable').DataTable({
          processing: true,
          serverSide: true,
          autoWidth: false,
          language: {
            url: '{{ asset('vendor/datatables/lang/Indonesian.json') }}'
          },
          ajax: {
            url: '{{ route('sales.index') }}',
            data: function (d) {
              d.status = $('select[name=status]').val()
            }
          },
          columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'invoice', name: 'invoice'},
            {data: 'tenant.name', name: 'tenant.name'},
            {data: 'customer_name', name: 'customer_name', defaultContent : '-'},
            {data: null, name: 'price_total', render: function ( data, type, row ) {
              return 'Rp ' + numberFormat(data.price_total);
            }},
            {data: null, name: 'status', render: function ( data, type, row ) {
              var badge = 'badge-warning';
              if (data.status == 200){
                badge = 'badge-success';  
              } else if (data.status == 300){
                badge = 'badge-primary';  
              } else if (data.status == 10){
                badge = 'badge-danger';  
              } 
              return '<span class="badge '+badge+'">'+data.display_status+'</span>';
            }},
            {data: 'action', name: 'action', orderable: false, searchable: false},
          ]
      });

      $('#btn-filter').click(function(){
        $('.datatable').DataTable().draw(true);
      });

      $(document).on('click','.js-submit-confirm', function(e){
          e.preventDefault();
          swal({
            title: 'Apakah anda yakin ingin menghapus?',
            text: 'Data yang sudah dihapus, tidak dapat dikembalikan!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $(this).closest('form').submit();
            } 
          });
      });

      @if(Session::has('order_notification.message'))
        $('#sendNotifModal').modal('show');
      @endif
    });

    function numberFormat(x) {
      return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
    }

    function sendNotif() {
      var message = textAreaNewLine();
      var phone = $('#phone-number').val();
      var param = 'phone=' + phone + '&text=' + message;
      var url = "https://api.whatsapp.com/send?"+param;
      window.open(url, '_blank');
      $('#sendNotifModal').modal('hide');
    }

    function textAreaNewLine(textareaID) {
      if (!textareaID) var textareaID = "#text";
      var textarea = $(textareaID);
      return $(textarea).val().replace(/(\r\n|\n)/g, "%0a");       
    }
  </script>
  @include('partials._toast')
@endsection