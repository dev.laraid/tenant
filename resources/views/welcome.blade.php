
@extends('layouts.main')

@section('title', config('app.name'))

@section('content_header')
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1 class="text-white">Daftar Menu</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item text-white active">Beranda</li>
      </ol>
    </div>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-3">
      <div class="card">
        <div class="card-header">
          Filter
        </div>
        <div class="card-body">
          <form id="frm-reject" action="{{ url('/') }}" method="get">
            <div class="form-group {{ $errors->has('reject_reason') ? ' has-error' : '' }}">
              <label class="d-block text-muted"><i class="fa fa-utensils"></i> Jenis Makanan</label>
              <select name="type" class="form-control select2">
                <option value="all">Semua</option>
                <option value="makanan" {{ $type == 'makanan' ? 'selected' : '' }}>Makanan</option>
                <option value="minuman" {{ $type == 'minuman' ? 'selected' : '' }}>Minuman</option>
                <option value="ice" {{ $type == 'ice' ? 'selected' : '' }}>Ice Cream</option>
              </select>
            </div>
            <div class="form-group">
              <label class="d-block text-muted"><i class="fa fa-store"></i> Tenant</label>
              @php
                $arr_tenant = explode(',', $tenant);
              @endphp
              @foreach ($tenants as $item)
                <div class="custom-control custom-checkbox">
                  <input class="custom-control-input cb-filter" type="checkbox" onchange="onChecked()" value="{{ $item->slug }}" id="cb_{{ $item->id }}" {{ in_array($item->slug, $arr_tenant) ? 'checked' : '' }}>
                  <label for="cb_{{ $item->id }}" class="custom-control-label text-muted">{{ $item->name }}</label>
                </div>
              @endforeach
            </div>
            <input type="hidden" name="tenant" id="tenant-filter" value="{{ $tenant }}">
            <button class="btn btn-sm btn-secondary">Filter</button>
          </form>
        </div>
      </div>
    </div>

    <div class="col-md-9">
      <div class="card card-solid">
        <div class="card-body">
          
          <div class="row">

            @foreach ($foods as $item)  
              <div class="col-md-4">
                <div class="card bg-light">
                  <img class="card-img-top center-cropped" src="{{asset('uploads/images/products/'.$item->image)}}" alt="Food">
                  
                  <div class="card-body">
                    <h5>{{ Str::limit($item->name, 23) }}</h5>
                    <h6 class="card-subtitle mb-2 text-muted text-md"><i class="fa fa-store"></i> {{ $item->tenant->name }}</h6>
                    <p class="card-text text-sm">
                      {{ $item->description }}
                    </p>
                    <div class="options d-flex flex-fill">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <button class="btn btn-dark btn-sm" id="minus-btn" onclick="decQty({{ $item->id }})"><i class="fa fa-minus"></i></button>
                        </div>
                        <input type="number" id="qty_input_{{ $item->id }}" class="form-control form-control-sm text-center h5 qty_input" value="1" min="1">
                        <div class="input-group-prepend">
                          <button class="btn btn-dark btn-sm" id="plus-btn" onclick="incQty({{ $item->id }})"><i class="fa fa-plus"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card-footer">
                    <div class="buy d-flex justify-content-between align-items-center">
                      <div class="price text-success">
                        <h5>IDR {{ toKFormat($item->price) }}</h5>
                      </div>

                      <a href="javascript:void(0)" class="btn btn-sm btn-primary" onclick="addCart({{ $item->id }})"><i class="fas fa-shopping-cart"></i> Beli</a>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
            
          </div>

          {{ $foods->appends(compact('type', 'tenant', 'q'))->links() }}
          
        </div>
      </div>
    </div>
  </div>

  <form id="order-form" action="{{ route('cart.add') }}" method="POST" style="display: none;">
    @csrf
    <input type="hidden" id="food_id" name="food_id">
    <input type="hidden" id="food_qty" name="food_qty">
  </form>
  
@endsection

@section('css')
    <style>
      .center-cropped {
        background-position: center center;
        background-repeat: no-repeat;
        overflow: hidden;
        height: 190px;
        width: 255px;
      }
    </style>
@endsection

@section('js')
    <script> 
      $(document).ready(function(){
        $('.qty_input').prop('disabled', true);
        $('.select2').select2();
      });

      var incQty = function(id) {
        let $input = $('#qty_input_'+id);
        let qty = parseInt($input.val()) + 1 
        if (qty > 30) {
          swal(
              'Maaf!',
              'Maximum jumlah item adalah 30 pcs',
              'error'
          );
          return;
        }
        $input.val(qty);
      }

      var decQty = function(id) {
        let $input = $('#qty_input_'+id);
        $input.val(parseInt($input.val()) - 1 );
        if ($input.val() == 0) {
          $input.val(1);
        }
      }

      function onChecked() {  
        var checkedVals = $('.cb-filter:checkbox:checked').map(function() {
          return this.value;
        }).get();
        $('#tenant-filter').val(checkedVals.join(","));
      }

      function addCart(pid = null) {
        if(pid != null) {
          $('#food_id').val(pid);
          $('#food_qty').val($('#qty_input_'+pid).val());
        }
        $('#order-form').submit();
      }

       @if(Session::has('swal_notification.message'))
        var type = "{{ Session::get('swal_notification.level', 'info') }}";
        switch(type){
          case 'success':
            swal(
              'Sukses!',
              '{{ Session::get('swal_notification.message') }}',
              'success'
            );
            break;

          case 'error':
            swal(
              'Gagal!',
              '{{ Session::get('swal_notification.message') }}',
              'error'
            );
            break;

          default:
            break;
        }
      @endif

    </script>
@endsection