<script>
    @if(Session::has('flash_notification.message'))
        var type = "{{ Session::get('flash_notification.level', 'info') }}";
        toastr.options.positionClass = 'toast-bottom-right';
        switch(type){
            case 'info':
                toastr.info("{{ Session::get('flash_notification.message') }}");
                break;
            
            case 'warning':
                toastr.warning("{{ Session::get('flash_notification.message') }}");
                break;

            case 'success':
                toastr.success("{{ Session::get('flash_notification.message') }}");
                break;

            case 'error':
                toastr.error("{{ Session::get('flash_notification.message') }}");
                break;
        }
    @endif
</script>