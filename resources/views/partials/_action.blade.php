<form class="delete" action="{{ $form_url ?? '' }}" method="post" id="delete-form">
    {{csrf_field()}}
    {{method_field('delete')}}
    @isset ($show_url)
        <a href="{{ $show_url }}" class="btn btn-circle btn-sm btn-info" title="Lihat Detail"><i class="fa fa-fw fa-eye"></i></a>
    @endisset
    @isset ($edit_url)
    <a href="{{ $edit_url }}" class="btn btn-circle btn-sm btn-warning" title="Edit Data"><i class="fa fa-fw fa-edit"></i></a>
    @endisset
    @isset ($form_url)
        <button type="submit" value="Delete" class="btn btn-circle btn-sm btn-danger js-submit-confirm" title="Hapus Data">
            <i class="fa fa-trash"></i>
        </button>
    @endisset
    @isset ($pay_url)
        @if ($model->is_paid == 0 && $model->status == 100)
            <a href="{{ $pay_url }}" class="btn btn-circle btn-sm btn-success" title="Lakukan Pembayaran"><i class="fa fa-fw fa-dollar-sign"></i></a>
        @endif
    @endisset
    @isset ($process_url)
        @if ($model->is_paid == 1 && $model->status == 200)
            <a href="{{ $process_url }}" class="btn btn-circle btn-sm btn-success" title="Proses Pesanan"><i class="fa fa-fw fa-cog"></i></a>
        @endif
    @endisset
</form>