<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tenant extends Model
{
    use SoftDeletes;

    protected $appends = ['display_status'];

    public function user()
    {
        return $this->belongsTo('App\User', 'owner_id');
    }

    public function scopeGenerateSlug($query, $title)
    {
        $g_slug = Str::slug($title);
        $slug_check = $this->where('slug', $g_slug)->count();
        if ($slug_check == 0) {
            $slug = $g_slug;
        } else {
            $check = 0;
            $unique = false;
            while ($unique == false) {
                $randomID = ++$check;
                $check = $this->where('slug', $g_slug . '-' . $randomID)->count();
                if ($check > 0) {
                    $unique = false;
                } else {
                    $unique = true;
                }
            }
            $slug = $g_slug . '-' . $randomID;
        }

        return $slug;
    }

    public function getDisplayStatusAttribute()
    {
        $result = '';
        $status = $this->attributes['status'];

        switch ($status) {
            case '100':
                $result = 'Aktif';
                break;
            case '10':
                $result = 'Tidak Aktif';
                break;

            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }
}
