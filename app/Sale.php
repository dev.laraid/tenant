<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    use SoftDeletes;

    protected $appends = ['display_status'];

    public function cashier()
    {
        return $this->belongsTo('App\User', 'cashier_id');
    }

    public function tenant()
    {
        return $this->belongsTo('App\Tenant', 'tenant_id');
    }

    public function details()
    {
        return $this->hasMany('App\SaleDetail');
    }

    public static function generateInvoice()
    {
        $start = '1022' . date('ymd') . '0001';
        $last = self::latest()->first();
        if ($last) {
            $start = $last->invoice;
        }
        $count = 2;
        $digits = 14;
        $result = array();
        for ($n = $start; $n < $start + $count; $n++) {
            $result[] = str_pad($n, $digits, "0", STR_PAD_LEFT);
        }
        return $result[1];
    }

    public function getDisplayStatusAttribute()
    {
        $result = '';
        $status = $this->attributes['status'];

        switch ($status) {
            case '100':
                $result = 'Menunggu Pembayaran';
                break;
            case '200':
                $result = 'Dibayar';
                break;
            case '300':
                $result = 'Selesai';
                break;
            case '10':
                $result = 'Dibatalkan';
                break;

            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }
}
