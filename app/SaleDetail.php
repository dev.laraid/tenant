<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaleDetail extends Model
{
    use SoftDeletes;

    public function sale()
    {
        return $this->belongsTo('App\Sale', 'sale_id');
    }

    public function food()
    {
        return $this->belongsTo('App\Food', 'food_id');
    }
}
