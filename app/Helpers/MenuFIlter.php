<?php

namespace App\Helpers;

use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;
use Auth;

class MenuFilter implements FilterInterface
{
    public function transform($item)
    {   
        if (Auth::user()) {
            if (isset($item['role']) && !Auth::user()->hasRole($item['role'])) {
                return false;
            }
        }

        return $item;
    }
}
