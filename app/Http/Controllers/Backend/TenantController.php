<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Yajra\Datatables\Datatables;
use App\Tenant;
use App\User;
use Session;

class TenantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $tenants = Tenant::with('user');

            if ($request->get('is_verification') != null) {
                if ($request->is_verification != 'all') {
                    $tenants->where('is_verification', $request->is_verification);
                }
            }

            $tenants = $tenants->select('tenants.*');

            return Datatables::of($tenants)
                ->addIndexColumn()
                ->addColumn('action', function ($tenant) {
                    return view('partials._action', [
                        'model'       => $tenant,
                        'show_url'    => route('tenant.show', $tenant->id),
                        'edit_url'    => route('tenant.edit', $tenant->id),
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('backend.tenants.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::where('role', 'tenant')->whereDoesntHave('tenant')->get();
        return view('backend.tenants.create')->with(compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'user_id' => 'required|integer',
            'description' => 'required|string',
        ]);

        $tenant = new Tenant;
        $tenant->owner_id = $request->user_id;
        $tenant->name = $request->name;
        $tenant->slug = Tenant::generateSlug($request->name);
        $tenant->description = $request->description;

        if ($request->hasFile('image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = uniqid() . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'tenant';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);

            // ganti field image dengan image yang baru
            $tenant->image = $filename;
        }
        $tenant->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Tenant berhasil didaftarkan"
        ]);

        return redirect()->route('tenant.show', $tenant->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tenant = Tenant::find($id);
        $id_crypt = Crypt::encrypt($id);
        return view('backend.tenants.show')->with(compact('tenant', 'id_crypt'));
    }

    public function verification(Request $request)
    {
        $id = Crypt::decrypt($request->tokenizer);

        $tenant = Tenant::find($id);
        $tenant->is_verification = 1;
        $tenant->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Tenant ini berhasil diverifikasi"
        ]);

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tenant = Tenant::find($id);
        return view('backend.tenants.edit')->with(compact('tenant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'status' => 'required|integer',
        ]);

        $tenant = Tenant::find($id);
        $tenant->name = $request->name;
        if ($tenant->name != $request->name) {
            $tenant->slug = Tenant::generateSlug($request->name);
        }
        $tenant->description = $request->description;
        $tenant->status = $request->status;

        if ($request->hasFile('image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = uniqid() . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'tenant';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);

            // ganti field image dengan image yang baru
            $tenant->image = $filename;
        }
        $tenant->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Tenant berhasil diubah"
        ]);

        return redirect()->route('tenant.index', $tenant->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
