<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Yajra\Datatables\Datatables;
use App\Tenant;
use App\Sale;
use App\SaleDetail;
use Session;
use Auth;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $sales = Sale::with('cashier', 'tenant')->orderBy('id', 'DESC');

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $sales->where('status', $request->status);
                }
            }

            $sales = $sales->select('sales.*');

            return Datatables::of($sales)
                ->addIndexColumn()
                ->addColumn('action', function ($sale) {
                    return view('partials._action', [
                        'model'       => $sale,
                        'show_url'    => route('sale.show', $sale->id),
                        'pay_url'     => route('sale.pay', $sale->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('backend.sales.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sale = Sale::find($id);

        return view('backend.sales.show')->with(compact('sale'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sale = Sale::find($id);
        $sale->cashier_id = Auth::user()->id;
        $sale->status = 10;
        $sale->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Transaksi ini berhasil dibatalkan"
        ]);

        return redirect()->route('sale.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pay($id)
    {
        $sale = Sale::find($id);

        if ($sale->is_paid == 1){
            Session::flash("flash_notification", [
                "level" => "error",
                "message" => "Transaksi ini telah dibayar!"
            ]);

            return redirect()->route('sale.index');
        }

        return view('backend.sales.pay')->with(compact('sale'));
    }

    public function payStore(Request $request, $id)
    {
        $this->validate($request, [
            'paid' => 'required|numeric',
            'cashback' => 'required|numeric',
        ]);

        $sale = Sale::find($id);
        $sale->paid = $request->paid;
        $sale->cashback = $request->cashback;
        $sale->cashier_id = Auth::user()->id;
        $sale->is_paid = 1;
        $sale->status = 200;
        $sale->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Pembayaran berhasil, pesanan telah dilanjutkan ke penjual"
        ]);

        return redirect()->route('sale.index');
    }
}
