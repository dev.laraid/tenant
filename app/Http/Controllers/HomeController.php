<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Food;
use App\Sale;
use App\Tenant;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        $auth = \Auth::user()->role;

        if ($auth != 'tenant') {
            return $this->admin();
        } else {
            return $this->member();
        }
    }

    public function admin()
    {
        $tenant_id = 6;

        $tenant = Tenant::count();
        $sale = Sale::count();
        $income_today = Sale::whereIsPaid(1)->whereDate('created_at', date('Y-m-d'))->sum('price_total');
        $income_all = Sale::whereIsPaid(1)->sum('price_total');
        

        $data = [
            'tenant' => $tenant,
            'order' => $sale,
            'income_today' => rupiah($income_today),
            'income_all' => rupiah($income_all)
        ];

        return view('backend.home')->with(compact('data'));
    }

    public function member()
    {
        /* if ($auth = \Auth::user()->is_verified == 0) {
            return view('frontend.accounts.unverified');
        } */
        $tenant_id = Auth::user()->tenant->id;

        $food = Food::where('tenant_id', $tenant_id)->count();
        $sale = Sale::where('tenant_id', $tenant_id)->get();
        $income_today = Sale::where('tenant_id', $tenant_id)->whereIsPaid(1)->whereStatus(300)->whereDate('created_at', date('Y-m-d'))->sum('price_total');
        $income_all = Sale::where('tenant_id', $tenant_id)->whereIsPaid(1)->whereStatus(300)->sum('price_total');

        $tenant = Tenant::find($tenant_id);
        $tenant->balances = $income_all;
        $tenant->save();


        $data = [
            'product' => $food,
            'order' => $sale->count(),
            'income_today' => rupiah($income_today),
            'income_all' => rupiah($income_all)
        ];

        return view('frontend.home')->with(compact('data'));
    }
}
