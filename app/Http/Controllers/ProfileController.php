<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Tenant;
use Session;

class ProfileController extends Controller
{
    public function show()
    {
        $user = \Auth::user();

        if ($user->role == 'cashier') {
            return view('backend.users.profile', ['user' => $user]);
        } else {
            $tenant = Tenant::where('owner_id', $user->id)->first();
            return view('frontend.accounts.profile')->with(compact('user', 'tenant'));
        }
    }

    public function update(Request $request)
    {
        $id = \Auth::user()->id;
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|unique:users,email,' . $id,
            'phone' => 'required|numeric',
        ]);

        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;

        if ($request->hasFile('avatar')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('avatar');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = uniqid() . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'avatars';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);
            // hapus image lama, jika ada
            if ($user->avatar) {
                $old_image = $user->avatar;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'avatars'
                . DIRECTORY_SEPARATOR . $user->avatar;
                try {
                    File::delete($filepath);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }
            }
            // ganti field image dengan image yang baru
            $user->avatar = $filename;
        }
        $user->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Profil berhasil diubah"
        ]);

        return redirect()->route('profile.show');
    }

    public function updateUser(Request $request)
    {
        $id = \Auth::user()->id;
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|unique:users,email,' . $id,
            'tenant_name' => 'required|string|max:255',
            'tenant_description' => 'required|string',
        ]);

        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;

        if ($request->hasFile('avatar')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('avatar');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = uniqid() . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'avatars';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);
            // hapus image lama, jika ada
            if ($user->avatar) {
                $old_image = $user->avatar;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'avatars'
                . DIRECTORY_SEPARATOR . $user->avatar;
                try {
                    File::delete($filepath);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }
            }
            // ganti field image dengan image yang baru
            $user->avatar = $filename;
        }
        $user->save();

        $tenant = Tenant::where('owner_id', $id)->first();
        $tenant->name = $request->tenant_name;
        $tenant->description = $request->tenant_description;
        $tenant->slug = Tenant::generateSlug($request->name);

        if ($request->hasFile('image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = uniqid() . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'tenant';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);
            // hapus image lama, jika ada
            if ($tenant->image) {
                $old_image = $tenant->image;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'tenant'
                . DIRECTORY_SEPARATOR . $tenant->image;
                try {
                    File::delete($filepath);
                } catch (FileNotFoundException $e) {
                    // File sudah dihapus/tidak ada
                }
            }
            // ganti field image dengan image yang baru
            $tenant->image = $filename;
        }
        $tenant->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Informasi akun berhasil diubah"
        ]);

        return redirect()->route('profile.show');
    }
}
