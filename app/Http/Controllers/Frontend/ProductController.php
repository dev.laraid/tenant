<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Datatables;
use App\SaleDetail;
use App\Food;
use Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $tenant_id = \Auth::user()->tenant->id;

            $products = Food::where('tenant_id', $tenant_id);

            if ($request->input('status') != null) {
                if ($request->status != 'all') {
                    $products->whereStatus($request->status);
                }
            }

            $products = $products->select('food.*');

            return Datatables::of($products)
                ->addIndexColumn()
                ->addColumn('action', function ($product) {
                    return view('partials._action', [
                        'model'           => $product,
                        'form_url'        => route('product.destroy', $product->id),
                        'edit_url'        => route('product.edit', $product->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        return view('frontend.products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'type' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|numeric',
            'image' => 'required|image',
        ]);

        $tenant_id = \Auth::user()->tenant->id;

        $product = new Food;
        $product->tenant_id = $tenant_id;
        $product->name = $request->name;
        $product->type = $request->type;
        $product->description = $request->description;
        $product->price = $request->price;

        if ($request->hasFile('image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = uniqid() . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'products';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);

            $product->image = $filename;
        }
        $product->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Produk $product->name berhasil dibuat"
        ]);

        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Food::find($id);

        return view('frontend.products.edit')->with(compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'type' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|numeric',
        ]);

        $tenant_id = \Auth::user()->tenant->id;

        $product = Food::find($id);
        $product->tenant_id = $tenant_id;
        $product->name = $request->name;
        $product->type = $request->type;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->status = $request->status;

        if ($request->hasFile('image')) {
            // megnambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = uniqid() . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'products';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);

            $product->image = $filename;
        }
        $product->save();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Produk $product->name berhasil diubah"
        ]);

        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Food::find($id);

        $check_sale = SaleDetail::where('food_id', $id)->count();
        if ($check_sale > 0) {
            Session::flash("flash_notification", [
                "level" => "warning",
                "message" => "Tidak dapat menghapus produk ini, karena produk ini telah memiliki histori data transaksi."
            ]);
            return redirect()->back();
        }

        /* if ($product->image) {
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'products'
                . DIRECTORY_SEPARATOR . $product->avatar;
            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
        } */

        $product->delete();

        Session::flash("flash_notification", [
            "level" => "success",
            "message" => "Berhasil menghapus data"
        ]);

        return redirect()->route('product.index');
    }
}
