<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Tenant;
use App\Food;
use App\Sale;
use App\SaleDetail;
use Session;

class PageController extends Controller
{
    public function landing(Request $request)
    {
        $tenants = Tenant::where('is_verification', 1)->where('status', 100)->get();
        
        $q = $request->q ?? '';
        $type = $request->type ?? '';
        $tenant = $request->tenant ?? '';

        $foods = Food::where('status', 100)->whereHas('tenant', function($query){
            $query->where('status', 100);
        });

        if ($request->has('q')) {
            if ($request->q != 'all') {
                $foods->where('name', 'like', '%' . $request->q . '%');
            }
        }

        if ($request->has('type')) {
            if ($request->type != 'all') {
                $foods->where('type', 'like', '%' . $request->type . '%');
            }
        }

        if ($request->has('tenant')) {
            if ($request->tenant != '') {
                $foods->whereHas('tenant', function($q) use($request){
                    $tenants = explode(',', $request->tenant);
                    foreach ($tenants as $key => $item) {
                        if ($key == 0) {
                            $q->where('slug', 'like', '%' . $item . '%');
                        } else {
                            $q->orWhere('slug', 'like', '%' . $item . '%');
                        }
                    }
                });
            }
        }

        $foods = $foods->paginate(9);

        return view('welcome')->with(compact('tenants', 'foods', 'q', 'type', 'tenant'));
    }

    public function addToCart(Request $request)
    {
        $id = $request->food_id;
        $food = Food::find($id);

        $cart = session()->get('cart');

        $tenant_id = $food->tenant_id;
        $tenant_name = $food->tenant->name;
        // if cart is empty then this the first product
        if (!$cart) {
            $cart = [
                $tenant_id => [
                    'tenant_name' => $tenant_name,
                    'data' => [
                        $id => [
                            "id" => $food->id,
                            "name" => $food->name,
                            "quantity" => $request->food_qty,
                            "image_url" => asset('uploads/images/products/' . $food->image),
                            "price" => $food->price
                        ]
                    ]
                ]
            ];

            session()->put('cart', $cart);

            Session::flash("swal_notification", [
                "level" => "success",
                "message" => "Item berhasil ditambahkan ke list order"
            ]);

            return redirect()->back();
        }

        // if cart not empty then check if this product exist then increment quantity
        if (isset($cart[$tenant_id]['data'][$id])) {

            if ($request->food_qty == 1) {
                $cart[$tenant_id]['data'][$id]['quantity']++;
            } else {
                $cart[$tenant_id]['data'][$id]['quantity'] = $request->food_qty;
            }

            session()->put('cart', $cart);

            Session::flash("swal_notification", [
                "level" => "success",
                "message" => "Item berhasil ditambahkan ke list order"
            ]);

            return redirect()->back();
        }

        // if item not exist in cart then add to cart with quantity = 1
        $cart[$tenant_id]['tenant_name'] = $tenant_name;
        $cart[$tenant_id]['data'][$id] = [
            "id" => $food->id,
            "name" => $food->name,
            "quantity" => $request->food_qty,
            "image_url" => asset('uploads/images/products/' . $food->image),
            "price" => $food->price
        ];

        session()->put('cart', $cart);

        Session::flash("swal_notification", [
            "level" => "success",
            "message" => "Item berhasil ditambahkan ke list order"
        ]);
        return redirect()->back();
    }

    public function updateCart(Request $request)
    {
        $status = 0;
        if ($request->tenant_id and $request->food_id and $request->quantity) {
            $cart = session()->get('cart');

            $cart[$request->tenant_id]['data'][$request->food_id]['quantity'] = $request->quantity;

            session()->put('cart', $cart);

            $status = 1;
        }
        if ($status == 1) {
            Session::flash("swal_notification", [
                "level" => "success",
                "message" => "Item berhasil diupdate"
            ]);
        } else {
            Session::flash("swal_notification", [
                "level" => "error",
                "message" => "Gagal terjadi kesalahan!"
            ]);
        }

        return redirect()->back();
    }

    public function removeItemCart(Request $request)
    {
        $status = 0;
        if ($request->tenant_id and $request->food_id) {

            $cart = session()->get('cart');
            if (isset($cart[$request->tenant_id]['data'][$request->food_id])) {

                unset($cart[$request->tenant_id]['data'][$request->food_id]);

                if (empty($cart[$request->tenant_id]['data'])) {
                    unset($cart[$request->tenant_id]);
                }

                session()->put('cart', $cart);

                $status = 1;
            }
        }
        if ($status == 1) {
            Session::flash("swal_notification", [
                "level" => "success",
                "message" => "Item berhasil dihapus"
            ]);
        } else {
            Session::flash("swal_notification", [
                "level" => "error",
                "message" => "Gagal terjadi kesalahan!"
            ]);
        }
        return redirect()->back();
    }

    public function indexCart()
    {
        $carts = session('cart') ?? [];
        //dd($carts);
        return view('frontend.pages.cart')->with(compact('carts'));
    }

    public function checkoutStore(Request $request, $tenant_id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'phone' => 'required|numeric|digits_between:9,15',
        ]);

        //dd($request->all());

        $cart = session('cart')[$tenant_id] ?? [];
        if (empty($cart)) {
            abort(404);
        }

        $sale = new Sale();
        $sale->invoice = Sale::generateInvoice();
        $sale->tenant_id = $tenant_id;
        $sale->customer_name = $request->name;
        $sale->customer_phone = $request->phone;
        $sale->customer_note = $request->note ?? '-';
        $sale->food_total = 0;
        $sale->price_total = 0;
        $sale->save();

        $price_total = 0;
        $food_total = 0;
        $subtotal = 0;
        foreach ($cart['data'] as $key => $item) {
            $detail = new SaleDetail();
            $detail->sale_id = $sale->id;
            $detail->food_id = $item['id'];
            $detail->price = $item['price'];
            $detail->qty = $item['quantity'];
            $detail->subtotal = $item['price'] * $item['quantity'];
            $detail->save();

            $price_total = $item['quantity'] * $item['price'];
            $subtotal += $price_total;
            $food_total += $item['quantity'];
        }

        $sale->food_total = $food_total;
        $sale->price_total = $subtotal;
        $sale->save();

        // remove cart
        $carts = session('cart');
        unset($carts[$request->tenant_id]);
        session()->put('cart', $carts);

        Session::flash("swal_notification", [
            "level" => "success",
            "message" => "Checkout berhasil dengan nomor pesanan $sale->invoice, silahkan lakukan pembayaran di kasir ya :)"
        ]);

        return redirect()->route('landing');
        //return redirect()->route('confirm', $sale->invoice);
    }
}
