<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Yajra\Datatables\Datatables;
use App\Tenant;
use App\Sale;
use App\SaleDetail;
use Session;
use Auth;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tenant_id = \Auth::user()->tenant->id;

        if ($request->ajax()) {

            $sales = Sale::with('cashier', 'tenant')->where('tenant_id', $tenant_id);

            if ($request->get('status') != null) {
                if ($request->status != 'all') {
                    $sales->where('status', $request->status);
                }
            }

            $sales = $sales->select('sales.*');

            return Datatables::of($sales)
                ->addIndexColumn()
                ->addColumn('action', function ($sale) {
                    return view('partials._action', [
                        'model'       => $sale,
                        'show_url'    => route('sales.show', $sale->id),
                        'process_url'    => route('sales.process', $sale->id)
                    ]);
                })
                ->escapeColumns([])
                ->make(true);
        }

        $tmp = Sale::where('tenant_id', $tenant_id)->get();

        $data = [
            'pending' => $tmp->where('status', 100)->count(),
            'paid' => $tmp->where('status', 200)->count(),
            'done' => $tmp->where('status', 300)->count(),
            'reject' => $tmp->where('status', 10)->count()
        ];
        
        return view('frontend.sales.index')->with(compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sale = Sale::find($id);

        return view('frontend.sales.show')->with(compact('sale'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function process($id)
    {
        $sale = Sale::find($id);

        return view('frontend.sales.process')->with(compact('sale'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sale = Sale::find($id);
        $sale->status = 300;
        $sale->save();

        $customer = $sale->customer_name;
        $tenant = $sale->tenant->name;
        $invoice = $sale->invoice;

        $message = "Hallo $customer\nPesanan anda dari $tenant telah siap.\nSilahkan diambil sendiri ya.\nNo.Invoice : $invoice";
        $phone = preg_replace('/^0/','+62',$sale->customer_phone);

        Session::flash(
            "order_notification",
            [
                "message" => $message,
                "phone" => $phone
            ]
        );

        Session::flash(
            "flash_notification", [
                "level" => "success",
                "message" => "Transaksi ini berhasil diproses"
            ]
        );

        return redirect()->route('sales.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
