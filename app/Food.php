<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Food extends Model
{
    use SoftDeletes;

    protected $appends = ['display_status'];

    public function tenant()
    {
        return $this->belongsTo('App\Tenant', 'tenant_id');
    }

    public function getDisplayStatusAttribute()
    {
        $result = '';
        $status = $this->attributes['status'];

        switch ($status) {
            case '100':
                $result = 'Aktif';
                break;
            case '10':
                $result = 'Non Aktif';
                break;

            default:
                $result = 'Tidak Ada';
                break;
        }

        return $result;
    }
}
