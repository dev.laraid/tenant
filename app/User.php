<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tenant()
    {
        return $this->hasOne('App\Tenant', 'owner_id');
    }

    public function adminlte_image()
    {
        $avatar = \Auth::user()->avatar;
        return $avatar != null ? asset('uploads/images/avatars/'.$avatar) : 'https://picsum.photos/300/300';
    }

    public function adminlte_desc()
    {
        return 'That\'s a nice guy';
    }

    public function adminlte_profile_url()
    {
        return 'profile/username';
    }

    public function hasRole($name)
    {
        if ($this->role === $name) return true;

        return false;
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($user) {
            $user->tenant()->delete();
        });
    }
}
