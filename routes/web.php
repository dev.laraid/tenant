<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend\PageController@landing')->name('landing');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/account/profile', 'ProfileController@show')->name('profile.show');
Route::put('/account/profile', 'ProfileController@update')->name('profile.update');
Route::put('/account/profile/user', 'ProfileController@updateUser')->name('profile.update.user');

Route::post('/buy/cart', 'Frontend\PageController@addToCart')->name('cart.add');
Route::get('/buy/cart', 'Frontend\PageController@indexCart')->name('cart.index');
Route::post('/buy/checkout/{tenant_id}', 'Frontend\PageController@checkoutStore')->name('checkout.store');
Route::post('/buy/cart/remove', 'Frontend\PageController@removeItemCart')->name('cart.remove');
Route::post('/buy/cart/update', 'Frontend\PageController@updateCart')->name('cart.update');

Route::group(['prefix' => 'main'], function () {
    Route::group(['middleware' => ['auth', 'role:cashier']], function () {
        Route::get('user', 'Backend\UserController@index')->name('user.index');
        Route::post('user', 'Backend\UserController@store')->name('user.store');
        Route::get('user/create', 'Backend\UserController@create')->name('user.create');
        Route::get('user/{id}/edit', 'Backend\UserController@edit')->name('user.edit');
        Route::put('user/{id}/update', 'Backend\UserController@update')->name('user.update');
        Route::delete('user/{id}', 'Backend\UserController@destroy')->name('user.destroy');
    
        Route::resource('tenant', 'Backend\TenantController', ['only' => ['index', 'show', 'create', 'store', 'update', 'edit']]);
        Route::post('tenant.verification', 'Backend\TenantController@verification')->name('tenant.verification');
        Route::resource('sale', 'Backend\SaleController', ['only' => ['index', 'show', 'update']]);
        Route::get('sale/{id}/pay', 'Backend\SaleController@pay')->name('sale.pay');
        Route::post('sale/{id}/pay', 'Backend\SaleController@payStore')->name('sale.pay.store');
    });

    Route::group(['middleware' => ['auth', 'role:tenant']], function () {
        Route::resource('product', 'Frontend\ProductController');
        Route::resource('sales', 'Frontend\SaleController', ['only' => ['index', 'show', 'update']]);
        Route::get('sales/{id}/process', 'Frontend\SaleController@process')->name('sales.process');
    });

});