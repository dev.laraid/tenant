/*
 Navicat Premium Data Transfer

 Source Server         : laragon-mysql
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : tenant

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 18/07/2020 20:28:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for food
-- ----------------------------
DROP TABLE IF EXISTS `food`;
CREATE TABLE `food`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tenant_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `price` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 100,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of food
-- ----------------------------
INSERT INTO `food` VALUES (2, 1, 'Mie Ayam Bangka', 'makanan', 'Mie Ayam Bangka Original Lezat', 15000, '5f0aec851b732.jpg', 100, '2020-07-12 17:49:19', '2020-07-12 17:57:09', NULL);
INSERT INTO `food` VALUES (3, 1, 'Mie Ayam Baso', 'makanan', 'Mie ayam baso urat, cincang, telur', 16000, '5f0aecad9d7ca.jpg', 100, '2020-07-12 17:57:49', '2020-07-12 17:57:49', NULL);
INSERT INTO `food` VALUES (4, 1, 'Mie Ayam Baso Pangsit', 'makanan', 'Mie Ayam Baso Pangsit Lezat', 17000, '5f0aece6941ea.jpg', 100, '2020-07-12 17:58:46', '2020-07-12 17:58:46', NULL);
INSERT INTO `food` VALUES (5, 1, 'Kwetiaw', 'makanan', 'Kwetiaw Lezat dan Nikmat', 14000, '5f0aed08af02f.jpg', 100, '2020-07-12 17:59:20', '2020-07-12 17:59:20', NULL);
INSERT INTO `food` VALUES (6, 2, 'Ice Cream Green Tea', 'ice', 'Ice Cream  dengan rasa teh hijau', 15000, '5f1289b93ec99.jpg', 100, '2020-07-18 12:33:45', '2020-07-18 12:34:17', NULL);
INSERT INTO `food` VALUES (7, 2, 'Ice cream Cho Chip', 'ice', 'Ice cream Cho Chio Lezat', 12000, '5f128a2e87b22.jpg', 100, '2020-07-18 12:35:42', '2020-07-18 12:35:42', NULL);
INSERT INTO `food` VALUES (8, 2, 'Ice Cream Durian', 'ice', 'Ice Cream rasa durian mantap', 13000, '5f128a4eb3f09.jpg', 100, '2020-07-18 12:36:14', '2020-07-18 12:36:14', NULL);
INSERT INTO `food` VALUES (9, 2, 'Ice Cream Goreng', 'ice', 'Ice Cream nya digoreng loh', 8000, '5f128a72a161e.jpg', 100, '2020-07-18 12:36:50', '2020-07-18 12:36:50', NULL);
INSERT INTO `food` VALUES (10, 2, 'Neopolitan', 'ice', 'Pensaran? coba aja beli', 12000, '5f128aaacbdb6.jpg', 100, '2020-07-18 12:37:46', '2020-07-18 12:37:46', NULL);
INSERT INTO `food` VALUES (11, 3, 'Nasi Ayam Jeletot', 'makanan', 'nasi ayam tahu sambel', 16000, '5f12e81981a89.jpg', 100, '2020-07-18 19:16:25', '2020-07-18 19:19:28', NULL);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2020_07_08_232558_create_tenants_table', 2);
INSERT INTO `migrations` VALUES (5, '2020_07_08_232627_create_food_table', 2);
INSERT INTO `migrations` VALUES (6, '2020_07_08_232643_create_sales_table', 2);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sale_details
-- ----------------------------
DROP TABLE IF EXISTS `sale_details`;
CREATE TABLE `sale_details`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sale_id` bigint(20) UNSIGNED NOT NULL,
  `food_id` bigint(20) UNSIGNED NOT NULL,
  `price` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `qty` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `subtotal` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sale_details
-- ----------------------------
INSERT INTO `sale_details` VALUES (1, 4, 3, 16000, 1, 16000, '2020-07-17 23:39:44', '2020-07-17 23:39:44', NULL);
INSERT INTO `sale_details` VALUES (2, 4, 4, 17000, 2, 34000, '2020-07-17 23:39:44', '2020-07-17 23:39:44', NULL);
INSERT INTO `sale_details` VALUES (3, 4, 2, 15000, 1, 15000, '2020-07-17 23:39:44', '2020-07-17 23:39:44', NULL);
INSERT INTO `sale_details` VALUES (4, 5, 3, 16000, 1, 16000, '2020-07-17 23:41:38', '2020-07-17 23:41:38', NULL);
INSERT INTO `sale_details` VALUES (5, 6, 4, 17000, 1, 17000, '2020-07-18 00:11:24', '2020-07-18 00:11:24', NULL);
INSERT INTO `sale_details` VALUES (6, 6, 2, 15000, 1, 15000, '2020-07-18 00:11:24', '2020-07-18 00:11:24', NULL);
INSERT INTO `sale_details` VALUES (7, 6, 5, 14000, 3, 42000, '2020-07-18 00:11:24', '2020-07-18 00:11:24', NULL);
INSERT INTO `sale_details` VALUES (8, 7, 7, 12000, 2, 24000, '2020-07-18 16:34:21', '2020-07-18 16:34:21', NULL);
INSERT INTO `sale_details` VALUES (9, 7, 6, 15000, 1, 15000, '2020-07-18 16:34:21', '2020-07-18 16:34:21', NULL);
INSERT INTO `sale_details` VALUES (10, 8, 11, 16000, 2, 32000, '2020-07-18 19:22:17', '2020-07-18 19:22:17', NULL);
INSERT INTO `sale_details` VALUES (11, 9, 3, 16000, 2, 32000, '2020-07-18 19:24:26', '2020-07-18 19:24:26', NULL);
INSERT INTO `sale_details` VALUES (12, 10, 3, 16000, 2, 32000, '2020-07-18 20:05:27', '2020-07-18 20:05:27', NULL);
INSERT INTO `sale_details` VALUES (13, 10, 2, 15000, 3, 45000, '2020-07-18 20:05:27', '2020-07-18 20:05:27', NULL);
INSERT INTO `sale_details` VALUES (14, 11, 2, 15000, 2, 30000, '2020-07-18 20:08:58', '2020-07-18 20:08:58', NULL);
INSERT INTO `sale_details` VALUES (15, 12, 4, 17000, 2, 34000, '2020-07-18 20:12:38', '2020-07-18 20:12:38', NULL);

-- ----------------------------
-- Table structure for sales
-- ----------------------------
DROP TABLE IF EXISTS `sales`;
CREATE TABLE `sales`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `invoice` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenant_id` bigint(20) UNSIGNED NOT NULL,
  `cashier_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `customer_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_note` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `food_total` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `price_total` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `paid` int(10) UNSIGNED NULL DEFAULT NULL,
  `cashback` int(10) UNSIGNED NULL DEFAULT NULL,
  `is_paid` tinyint(1) NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 100,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sales
-- ----------------------------
INSERT INTO `sales` VALUES (4, '10222007180001', 1, 1, 'Iqbal', '089692825280', 'Makasih', 4, 65000, NULL, NULL, NULL, 10, '2020-07-17 23:39:44', '2020-07-18 11:55:57', NULL);
INSERT INTO `sales` VALUES (5, '10222007180002', 1, NULL, 'Iqbal', '089692825280', 'Makasih', 1, 16000, 16000, 0, 1, 300, '2020-07-17 23:41:38', '2020-07-18 16:15:31', NULL);
INSERT INTO `sales` VALUES (6, '10222007180003', 1, 1, 'Nabila', '089692825280', 'Meja 01', 5, 74000, NULL, NULL, NULL, 10, '2020-07-18 00:11:24', '2020-07-18 19:37:51', NULL);
INSERT INTO `sales` VALUES (7, '10222007180004', 2, 1, 'Nabila', '089692825280', 'Meja 01', 3, 39000, 40000, 1000, 1, 300, '2020-07-18 16:34:21', '2020-07-18 16:36:06', NULL);
INSERT INTO `sales` VALUES (8, '10222007180005', 3, 1, 'Nabila', '08980029525', 'Meja 01', 2, 32000, 50000, 18000, 1, 300, '2020-07-18 19:22:17', '2020-07-18 19:35:20', NULL);
INSERT INTO `sales` VALUES (10, '10222007180006', 1, NULL, 'Nabila', '08980029525', 'Terima kasih', 5, 77000, NULL, NULL, 0, 100, '2020-07-18 20:05:27', '2020-07-18 20:05:27', NULL);
INSERT INTO `sales` VALUES (11, '10222007180007', 1, 1, 'aris', '08980029525', 'Di Tunggu yaa orderan nya', 2, 30000, 50000, 20000, 1, 300, '2020-07-18 20:08:58', '2020-07-18 20:16:29', NULL);
INSERT INTO `sales` VALUES (12, '10222007180008', 1, 1, 'aris', '089691812701', 'Di tunggu yaa orderan nya', 2, 34000, 50000, 16000, 1, 300, '2020-07-18 20:12:38', '2020-07-18 20:20:16', NULL);

-- ----------------------------
-- Table structure for tenants
-- ----------------------------
DROP TABLE IF EXISTS `tenants`;
CREATE TABLE `tenants`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `owner_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_verification` tinyint(4) NOT NULL DEFAULT 0,
  `balances` bigint(20) UNSIGNED NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tenants
-- ----------------------------
INSERT INTO `tenants` VALUES (1, 3, 'Mie Ayam Bangka', 'pevita-pearce', 'Aneka macam mie ayam, bakso dan kwetiaw', '5f0aed7db592b.jpg', 1, 0, '2020-07-09 20:59:38', '2020-07-18 16:11:12', NULL);
INSERT INTO `tenants` VALUES (2, 6, 'Andalusia', 'andalusia', 'Menyediakan aneka macam ice cream', '5f1288c3f2c8d.jpg', 1, 0, '2020-07-18 12:29:39', '2020-07-18 12:29:54', NULL);
INSERT INTO `tenants` VALUES (3, 7, 'Bu Imas', 'bu-imas', 'Nasi Ayam Jeletot', NULL, 1, 0, '2020-07-18 19:11:48', '2020-07-18 19:12:13', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `role` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'tenant',
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Administrator', 'admin@gmail.com', '08765439884', NULL, '$2y$10$1CfHNvFjNqJsc5JmfoRqaugkouRyysNmE5HGlp2cwkPfSdRNZ1Fa6', '5f0731dabb327.jpg', 'admin', NULL, '2020-07-06 15:44:49', '2020-07-11 10:32:01');
INSERT INTO `users` VALUES (3, 'Pevita Pearce', 'pevita@gmail.com', '08765439884', NULL, '$2y$10$1CfHNvFjNqJsc5JmfoRqaugkouRyysNmE5HGlp2cwkPfSdRNZ1Fa6', '5f0731dabb327.jpg', 'tenant', '8yEWQzyn9Ph96Ee7gRSxWLOzSsv6gO2OUTaAzEioWVBd2aCSd7MaB14dgrO2', '2020-07-06 15:44:49', '2020-07-18 16:11:12');
INSERT INTO `users` VALUES (6, 'Raisa Andriana', 'raisa@gmail.com', '087657489990', NULL, '$2y$10$TWLFj8E1RLKi2n2DN4rOyu23cyMfMqYr9.n6.C2AueBNh7lA8FQ7C', '5f128491057c6.png', 'tenant', NULL, '2020-07-18 12:11:45', '2020-07-18 12:11:45');
INSERT INTO `users` VALUES (7, 'Bu Imas', 'imas@gmail.com', '08987856544', NULL, '$2y$10$qNs6/lFv4qMn4SOparRbpuDM22.Q7TwVB1KnRqwPqpw6vGLgFSzNK', NULL, 'tenant', NULL, '2020-07-18 19:10:22', '2020-07-18 19:10:22');

SET FOREIGN_KEY_CHECKS = 1;
