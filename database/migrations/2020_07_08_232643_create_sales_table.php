<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->id();
            $table->string('invoice');
            $table->unsignedBigInteger('tenant_id');
            $table->unsignedBigInteger('cashier_id')->nullable();
            $table->string('customer_name');
            $table->string('customer_phone', 16);
            $table->string('customer_note')->nullable();
            $table->unsignedInteger('food_total')->default(0);
            $table->unsignedInteger('price_total')->default(0);
            $table->unsignedInteger('paid')->default(0);
            $table->unsignedInteger('cashback')->default(0);
            $table->boolean('is_paid')->default(0);
            $table->integer('status')->default(100);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('sale_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('sale_id');
            $table->unsignedBigInteger('food_id');
            $table->unsignedInteger('price')->default(0);
            $table->unsignedInteger('qty')->default(0);
            $table->unsignedInteger('subtotal')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
        Schema::dropIfExists('sale_details');
    }
}
